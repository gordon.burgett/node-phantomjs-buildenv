FROM node:6.9.1
MAINTAINER "Gordon Burgett <gordon@gordonburgett.net>"

RUN apt-get update && apt-get install -y rsync zip

RUN curl -s -L -o /tmp/phantomjs.tar.bz2 https://github.com/Medium/phantomjs/releases/download/v2.1.1/phantomjs-2.1.1-linux-x86_64.tar.bz2 && \
      tar -xjf /tmp/phantomjs.tar.bz2 -C /tmp/ && \
      mv /tmp/phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/bin/

RUN npm install -g yarn
